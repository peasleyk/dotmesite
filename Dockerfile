FROM alpine:3.11.3

RUN apk update && \
    apk upgrade && \
    apk add --no-cache python3 && \
    pip3 install pipenv

# We need to run as root for dokku (port 80)
COPY . /app
WORKDIR /app

RUN pipenv install --skip-lock --system && \
    pip3 uninstall -y pipenv

EXPOSE 80
CMD ["gunicorn", "--workers=2", "--threads=4",  "app:app"]
