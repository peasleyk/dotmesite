from taskr import runners

DEFAULT = "all"
IMAGE_NAME = "home"
REPO_NAME = "dotmesite"
URL = "https://peasley.me"
PORT = 12345


def build():
    return runners.run(f"podman build . -t {IMAGE_NAME}")


def podman():
    return runners.run(f"podman run --rm --init -p {PORT}:80 {IMAGE_NAME}")


# Remove build artifacts, cache, etc.
def dev():
    return runners.run("python run.py")


# Run black
def fmt() -> bool:
    return runners.run("python -m ruff format .")


def lint() -> bool:
    return runners.run("python -m ruff check .")


# Runs all static analysis tools
def all() -> bool:
    return runners.run_conditional(fmt, types)


def types() -> bool:
    return runners.run("python -m mypy .")


# Launch this project in gitlab
def view():
    runners.run(f"firefox https://gitlab.com/peasleyk/{REPO_NAME}")


def site():
    runners.run(f"firefox {URL}")
