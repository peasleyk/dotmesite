let frame = 0;
const S = Math.sin;
const C = Math.cos;
const T = Math.tan;

function R(r, g, b, a) {
  var a = a === undefined ? 1 : a;
  return "rgba(" + (r | 0) + ", " + (g | 0) + ", " + (b | 0) + ", " + a + ")";
}

const algos = [
  // "r=400;c.width=r*4;λ=0;for(φ=a=Math.PI/2;φ>-a;φ-=1/r){λ+=a;x.lineTo(C(φ)*S(λ-t)*r+2*r,(C(t=t+1/r)*S(φ)+S(t)*C(φ)*C(λ-t))*r+r)}x.stroke()",
  "for(c.width=i=960;--i;x.ellipse(480,270-56*(j%2-.5)*y,r=32*(64-y*y)**.5,r/2,0,a=(j+t%1)/7,a+!j*7))j=i>>4,y=i%16-8;x.stroke()",
];

function u(t, dweet) {
  let c = document.getElementById("canvas");
  c.width = 1000;
  c.height = 1000;
  let x = (X = c.getContext("2d"));
  eval(dweet);
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function getDweet() {
  choice = Math.floor(Math.random() * Math.floor(algos.length));
  return algos[choice];
}

async function run(dweet) {
  while (true) {
    u(window.performance.now() / 1000, dweet);
    //don't kill our pc
    await sleep(25);
  }
}

document.addEventListener("DOMContentLoaded", function () {
  const dweet = getDweet();
  run(dweet);
});
