from flask import render_template, send_file

from app import app


@app.route("/")
@app.route("/index")
@app.route("/index.html")
def index():
    print("??")
    return render_template("index.html")


# Error codes
@app.errorhandler(404)
def e404(e):
    return render_template("404.html")


# files
# @app.route('/resume')
# @app.route('/Resume.pdf')
# def resume():
# 	return send_file('static/Resume.pdf')


@app.route("/resume")
def resume():
    return render_template("resume.html")


@app.route("/q")
def q():
    return send_file("static/quotes.txt")
